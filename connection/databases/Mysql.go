package databases

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func MySqlConnection() *sql.DB {

	db, err := sql.Open("mysql", "root:e11oxd1g1t4l@tcp(127.0.0.1:3306)/users")

	if err != nil {
		panic(err.Error())
	}

	return db
}

func MySqlRootConnection(schema string) *sql.DB {

	db, err := sql.Open("mysql", "root:e11oxd1g1t4l@tcp(127.0.0.1:3306)/" + schema)

	if err != nil {
		panic(err.Error())
	}

	return db
}

func MySqlCreateDatabaseConnection() *sql.DB {

	db, err := sql.Open("mysql", "root:e11oxd1g1t4l@tcp(127.0.0.1:3306)/")

	if err != nil {
		panic(err.Error())
	}

	return db
}