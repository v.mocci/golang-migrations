package connection

import (
	"database/sql"
	"gitlab.com/v.mocci/golang-migrations/connection/databases"
)

func ReturnConnectionString() *sql.DB {
	database := databases.MySqlConnection()
	return database
}

func ReturnRootConnectionString(schema string) *sql.DB {
	database := databases.MySqlRootConnection(schema)
	return database
}

func ReturnDatabaseCreationConn() *sql.DB{
	database := databases.MySqlCreateDatabaseConnection()
	return database
}
