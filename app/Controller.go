package migrationController

import (
	"os"
	"fmt"
	"gitlab.com/v.mocci/golang-migrations/app/Modules/CreateMigrations"
	"gitlab.com/v.mocci/golang-migrations/app/Modules/DeleteMigrations"
)

func RunMigrations(){
	if(os.Args[1] == "info"){
		fmt.Println("Use 'create' argument to create migrations and 'delete mydatabase' to DROP the inputed database")
	}

	if(os.Args[1] == "create"){
		createmigrations.CreateMigration()
	}

	if(os.Args[1] == "delete"){
		deletemigrations.DropDatabase(os.Args[2])
	}
}