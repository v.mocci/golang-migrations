package commons

type CreateTableQuery struct{
	Schema string
	NewTableName string
	NewTableColumns string
}

type CreateFKQuery struct {
	Schema string
	AlterTable string
	AddConstraint string
	ForeignKey string
	References string
	Extra string
}

type AlterTableQuery struct{
	Schema string
	AlterTable string
	NewColumnName string
	ColumnType string
	Default string
	Extra string
}