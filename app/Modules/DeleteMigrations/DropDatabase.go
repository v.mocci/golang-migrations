package deletemigrations

import (
	"fmt"
	"gitlab.com/v.mocci/golang-migrations/connection"
)

func DropDatabase(databaseName string) {
	database := connection.ReturnDatabaseCreationConn()

	createTableString := "DROP DATABASE " + databaseName;

	transaction, err := database.Begin()
	if err != nil {
		fmt.Println("erro aqui")
		panic(err.Error())
	}

	insertStmt, err := transaction.Prepare(createTableString)
	if err != nil {
		transaction.Rollback()
		panic("Erro na sintaxe SQL, verificar os dados inseridos.")
	}

	defer insertStmt.Close()

	result, err := insertStmt.Exec()
	result.LastInsertId()

	if err != nil {
		transaction.Rollback()
		panic(err.Error())
	}
	fmt.Println("Banco deletado com sucesso: " + databaseName)
}