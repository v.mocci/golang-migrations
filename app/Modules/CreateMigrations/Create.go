package createmigrations

import (
			"fmt"
			"gitlab.com/v.mocci/golang-migrations/connection"
	structs "gitlab.com/v.mocci/golang-migrations/app/Commons"
)

func CreateMigration(){
	
	CreateDatabase("terminal")

	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "processes", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, companyid INT (10) NOT NULL, exporterid INT (10) NOT NULL, workflowid INT (10) NOT NULL, contractnumber INT (20) NOT NULL, stuffingterminalid INT (10) NOT NULL, custombrokerid INT (10) NOT NULL, supervisorid INT (10) NOT NULL, fumigatorid INT (10) NOT NULL, shipownerid INT NOT NULL, containertypeid INT NOT NULL, containerquantity INT (10), detentiondays INT (10), depotterminal VARCHAR (50), voyagesid INT (10) NOT NULL, productid INT (10) NOT NULL, productquantity INT (10) NOT NULL, productweight INT (10) NOT NULL, createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "stuffingterminals", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, stuffingterminalname VARCHAR (100) NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "exporters", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, exportername VARCHAR (100) NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "fumigators", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, fumigatorname VARCHAR (100) NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "supervisors", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, supervisorname VARCHAR (100) NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "custombrokers", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, custombrokername VARCHAR (100) NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "products", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, producttype VARCHAR (50) NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "packlists", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, productid INT (10) NOT NULL, packlistpurchase VARCHAR (20), packlistsupplier VARCHAR (100), packlistlot INT (10), packlistbales INT (10), packlistbaleshc INT (10), packlistgrade INT (10), packlistsittingnf VARCHAR (50), packlistnetweight VARCHAR (20)"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "terminalsteps", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, processid INT NOT NULL, workflowstepId INT NOT NULL, createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, approved TINYINT DEFAULT 0 NOT NULL, approvedat DATETIME DEFAULT CURRENT_TIMESTAMP, reason VARCHAR(50)"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "terminaluserssteps", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, processid INT NOT NULL, terminalstepsid INT NOT NULL, userid INT, groupid INT, stepuserroleId INT NOT NULL, createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "terminals", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, terminalname VARCHAR(50) NOT NULL, terminaladress VARCHAR(100) NOT NULL, terminalcnpj VARCHAR(20) NOT NULL, createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "terminalcontacts", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, terminalid INT NOT NULL, terminalcontactname VARCHAR(50) NOT NULL, terminalcontactemail VARCHAR(100) NOT NULL, createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "terminalperiods", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, terminalid INT NOT NULL, terminalperiodname VARCHAR(50) NOT NULL, terminalperiodstarttime TIME NOT NULL, terminalperiodendtime TIME, createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "terminalservicegrids", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, terminalperiodid INT NOT NULL, terminalservicegridvacancies INT(10) NOT NULL, servicegridexceptionid INT NOT NULL, createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "servicegridexceptions", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, servicegridexceptiondescription VARCHAR(50) NOT NULL, createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "months", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(20) NOT NULL, createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL"})
	CreateTable(structs.CreateTableQuery{Schema: "terminal", NewTableName: "servicegridmonths", NewTableColumns: "id INT PRIMARY KEY AUTO_INCREMENT, servicegridid INT NOT NULL, monthid INT NOT NULL, createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL"})
	
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "processes", AddConstraint: "processes_stuffing_terminal_FK", ForeignKey: "(stuffingterminalid)", References: "stuffingterminals(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "processes", AddConstraint: "processes_exporters_FK", ForeignKey: "(exporterid)", References: "exporters(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "processes", AddConstraint: "processes_fumigators_FK", ForeignKey: "(fumigatorid)", References: "fumigators(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "processes", AddConstraint: "processes_supervisors_FK", ForeignKey: "(supervisorid)", References: "supervisors(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "processes", AddConstraint: "processes_custombrokers_FK", ForeignKey: "(custombrokerid)", References: "custombrokers(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "processes", AddConstraint: "processes_products_FK", ForeignKey: "(productid)", References: "products(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "processes", AddConstraint: "processes_workflows_FK", ForeignKey: "(workflowid)", References: "workflows.workflows(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "processes", AddConstraint: "processes_voyages_FK", ForeignKey: "(voyagesid)", References: "shipments.voyages(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "processes", AddConstraint: "processes_companies_FK", ForeignKey: "(companyid)", References: "users.companies(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "processes", AddConstraint: "processes_shipowners_FK", ForeignKey: "(shipownerid)", References: "shipments.vesselcompanies(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "processes", AddConstraint: "processes_containertypes_FK", ForeignKey: "(containertypeid)", References: "shipments.containertypes(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "terminalsteps", AddConstraint: "terminalsteps_processes_FK", ForeignKey: "(processid)", References: "processes(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "terminalsteps", AddConstraint: "terminalsteps_workflowsteps_FK", ForeignKey: "(workflowstepId)", References: "workflows.workflowsteps(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "terminaluserssteps", AddConstraint: "terminaluserssteps_processesFK", ForeignKey: "(processid)", References: "processes(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "terminaluserssteps", AddConstraint: "terminaluserssteps_groups_FK", ForeignKey: "(groupid)", References: " users.groups(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "terminaluserssteps", AddConstraint: "terminaluserssteps_users_FK", ForeignKey: "(userid)", References: "users.users(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "terminaluserssteps", AddConstraint: "terminaluserssteps_workflowstepuserroles_FK", ForeignKey: "(stepuserroleId)", References: "workflows.stepuserroles(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "terminaluserssteps", AddConstraint: "terminaluserssteps_workflowsteps_FK", ForeignKey: "(terminalstepsid)", References: "workflows.workflowsteps(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "servicegridmonths", AddConstraint: "servicegridmonths_servicegrids_FK", ForeignKey: "(servicegridid)", References: "terminalservicegrids(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "servicegridmonths", AddConstraint: "servicegridmonths_months_FK", ForeignKey: "(monthid)", References: "months(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "terminalservicegrids", AddConstraint: "terminalservicegrids_servicegridexceptions_FK", ForeignKey: "(servicegridexceptionid)", References: "servicegridexceptions(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "terminalperiods", AddConstraint: "terminalperiods_terminals_FK", ForeignKey: "(terminalid)", References: "terminals(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	CreateForeignKey(structs.CreateFKQuery{Schema:"terminal", AlterTable: "terminalcontacts", AddConstraint: "terminalcontacts_terminals_FK", ForeignKey: "(terminalid)", References: "terminals(id)", Extra: "ON DELETE RESTRICT ON UPDATE RESTRICT"})
	
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "custombrokers", NewColumnName: "createdat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "NOT NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "custombrokers", NewColumnName: "updatedat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "ON UPDATE CURRENT_TIMESTAMP NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "exporters", NewColumnName: "createdat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "NOT NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "exporters", NewColumnName: "updatedat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "ON UPDATE CURRENT_TIMESTAMP NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "fumigators", NewColumnName: "createdat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "NOT NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "fumigators", NewColumnName: "updatedat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "ON UPDATE CURRENT_TIMESTAMP NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "packlists", NewColumnName: "createdat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "NOT NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "packlists", NewColumnName: "updatedat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "ON UPDATE CURRENT_TIMESTAMP NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "products", NewColumnName: "createdat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "NOT NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "products", NewColumnName: "updatedat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "ON UPDATE CURRENT_TIMESTAMP NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "stuffingterminals", NewColumnName: "createdat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "NOT NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "stuffingterminals", NewColumnName: "updatedat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "ON UPDATE CURRENT_TIMESTAMP NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "supervisors", NewColumnName: "createdat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "NOT NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "supervisors", NewColumnName: "updatedat", ColumnType: "DATETIME", Default: "CURRENT_TIMESTAMP", Extra: "ON UPDATE CURRENT_TIMESTAMP NULL" })
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "processes", NewColumnName: "bookingid", ColumnType: "VARCHAR(10)", Extra: "NOT NULL AFTER fumigatorid"})
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "processes", NewColumnName: "unloaddatestart", ColumnType: "DATE", Extra: "NOT NULL AFTER depotterminal"})
	AddColumn(structs.AlterTableQuery{Schema: "terminal", AlterTable: "processes", NewColumnName: "unloaddateend", ColumnType: "DATE", Extra: "NOT NULL AFTER unloaddatestart"})
}

func CreateTable(tableData structs.CreateTableQuery)  {
	database := connection.ReturnRootConnectionString(tableData.Schema)
	createTableString := "CREATE TABLE IF NOT EXISTS " + tableData.NewTableName + " (" + tableData.NewTableColumns + ")";

	transaction, err := database.Begin()
	if err != nil {
		panic(err.Error())
	}

	insertStmt, err := transaction.Prepare(createTableString)
	if err != nil {
		transaction.Rollback()
		fmt.Println(createTableString)
		panic("Erro na sintaxe SQL, verificar os dados da tabela " + tableData.NewTableName)
	}

	defer insertStmt.Close()

	result, err := insertStmt.Exec()
	result.RowsAffected()
	
	if err != nil {
		transaction.Rollback()
		panic(err.Error())
	}

	fmt.Println("Tabela criada com sucesso: " + tableData.NewTableName)
}

func CreateDatabase(databaseName string) {
	database := connection.ReturnDatabaseCreationConn()
	createTableString := "CREATE DATABASE " + databaseName;

	transaction, err := database.Begin()
	if err != nil {
		panic(err.Error())
	}

	insertStmt, err := transaction.Prepare(createTableString)
	if err != nil {
		transaction.Rollback()
		panic("Erro na sintaxe SQL, verificar os dados inseridos")
	}

	defer insertStmt.Close()

	result, err := insertStmt.Exec()
	result.LastInsertId()

	if err != nil {
		transaction.Rollback()
		panic(err.Error())
	}
	fmt.Println("Banco criado com sucesso: " + databaseName)
}

func CreateForeignKey(ForeignKeyData structs.CreateFKQuery) {
	database := connection.ReturnDatabaseCreationConn()
	createTableString := "ALTER TABLE " + ForeignKeyData.Schema + "." + ForeignKeyData.AlterTable + " ADD CONSTRAINT " + ForeignKeyData.AddConstraint + " FOREIGN KEY " + ForeignKeyData.ForeignKey + " REFERENCES " + ForeignKeyData.References + " " + ForeignKeyData.Extra;

	transaction, err := database.Begin()
	if err != nil {
		panic(err.Error())
	}

	insertStmt, err := transaction.Prepare(createTableString)
	if err != nil {
		transaction.Rollback()
		panic("Erro na sintaxe SQL, verificar os dados inseridos.")
	}

	defer insertStmt.Close()

	result, err := insertStmt.Exec()
	result.RowsAffected()

	if err != nil {
		transaction.Rollback()
		panic(err.Error())
	}
	fmt.Println("Chave estrangeira criada com sucesso: ", ForeignKeyData.AddConstraint)
}

func AddColumn(alterTableData structs.AlterTableQuery){
	database := connection.ReturnDatabaseCreationConn()
	var addColumnString string
	if (alterTableData.Default == ""){
		addColumnString = "ALTER TABLE " + alterTableData.Schema + "." + alterTableData.AlterTable + " ADD " + alterTableData.NewColumnName +  " " + alterTableData.ColumnType + " " + alterTableData.Extra;

	}else{

		addColumnString = "ALTER TABLE " + alterTableData.Schema + "." + alterTableData.AlterTable + " ADD " + alterTableData.NewColumnName +  " " + alterTableData.ColumnType + " DEFAULT " + alterTableData.Default + " " + alterTableData.Extra;
	}

	transaction, err := database.Begin()
	if err != nil {
		panic(err.Error())
	}

	insertStmt, err := transaction.Prepare(addColumnString)
	if err != nil {
		transaction.Rollback()
		fmt.Println(addColumnString)
		panic("Erro na sintaxe SQL, verificar os dados inseridos.")
	}

	defer insertStmt.Close()

	result, err := insertStmt.Exec()
	result.RowsAffected()

	if err != nil {
		transaction.Rollback()
		panic(err.Error())
	}
	fmt.Println("Adição de coluna feita com sucesso. Adicionado: ", alterTableData.NewColumnName + " à tabela " + alterTableData.AlterTable)
}