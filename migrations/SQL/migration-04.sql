-- Criação dos campos createdat e updatedat nas tabelas que não contemplavam os mesmos --
ALTER TABLE terminal.custombrokers ADD createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL;
ALTER TABLE terminal.custombrokers ADD updatedat DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NULL;

ALTER TABLE terminal.exporters ADD createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL;
ALTER TABLE terminal.exporters ADD updatedat DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NULL;

ALTER TABLE terminal.fumigators ADD createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL;
ALTER TABLE terminal.fumigators ADD updatedat DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NULL;

ALTER TABLE terminal.packlists ADD createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL;
ALTER TABLE terminal.packlists ADD updatedat DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NULL;

ALTER TABLE terminal.products ADD createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL;
ALTER TABLE terminal.products ADD updatedat DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NULL;

ALTER TABLE terminal.stuffingterminals ADD createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL;
ALTER TABLE terminal.stuffingterminals ADD updatedat DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NULL;

ALTER TABLE terminal.supervisors ADD createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL;
ALTER TABLE terminal.supervisors ADD updatedat DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NULL;

-- Alteração dos nomes de ID para padronização do banco de dados --
ALTER TABLE terminal.custombrokers CHANGE custombrokerid id int auto_increment NOT NULL;
ALTER TABLE terminal.exporters CHANGE exporterid id int auto_increment NOT NULL;
ALTER TABLE terminal.fumigators CHANGE fumigatorid id int auto_increment NOT NULL;
ALTER TABLE terminal.months CHANGE monthid id int auto_increment NOT NULL;
ALTER TABLE terminal.packlists CHANGE packistid id int auto_increment NOT NULL;
ALTER TABLE terminal.processes CHANGE processid id int auto_increment NOT NULL;
ALTER TABLE terminal.products CHANGE productid id int auto_increment NOT NULL;
ALTER TABLE terminal.servicegridexceptions CHANGE servicegridexceptionid id int auto_increment NOT NULL;
ALTER TABLE terminal.servicegridmonths CHANGE servicegridmonthsid id int auto_increment NOT NULL;
ALTER TABLE terminal.stuffingterminals CHANGE stuffingterminalid id int auto_increment NOT NULL;
ALTER TABLE terminal.supervisors CHANGE supervisorid id int auto_increment NOT NULL;
ALTER TABLE terminal.terminalcontacts CHANGE terminalcontactsid id int auto_increment NOT NULL;
ALTER TABLE terminal.terminalperiods CHANGE terminalperiodsid id int auto_increment NOT NULL;
ALTER TABLE terminal.terminals CHANGE terminalid id int auto_increment NOT NULL;
ALTER TABLE terminal.terminalservicegrids CHANGE terminalservicegridid id int auto_increment NOT NULL;
ALTER TABLE terminal.terminalsteps CHANGE terminalstepsid id int auto_increment NOT NULL;
ALTER TABLE terminal.terminaluserssteps CHANGE terminalusersstepsid id int auto_increment NOT NULL;
