-- Adição dos campos createdat e updatedat na tabela de processos do terminal --
ALTER TABLE terminal.processes ADD createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL;
ALTER TABLE terminal.processes ADD updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL;
