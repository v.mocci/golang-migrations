-- --
-- Migration for terminal schema/databse --
-- --

CREATE DATABASE IF NOT EXISTS terminal;

USE terminal;

CREATE TABLE IF NOT EXISTS terminal.processes (
processid INT PRIMARY KEY AUTO_INCREMENT,
companyid INT (10) NOT NULL,
exporterid INT (10) NOT NULL,
workflowid INT (10) NOT NULL,
contractnumber INT (20) NOT NULL,
stuffingterminalid INT (10) NOT NULL,
custombrokerid INT (10) NOT NULL,
supervisorid INT (10) NOT NULL,
fumigatorid INT (10) NOT NULL,
shipownerid INT NOT NULL,
containertypeid INT NOT NULL,
containerquantity INT (10),
detentiondays INT (10),
depotterminal VARCHAR (50),
voyagesid INT (10) NOT NULL,
productid INT (10) NOT NULL,
productquantity INT (10) NOT NULL,
productweight INT (10) NOT NULL);


-- Stuffing Terminals table --
CREATE TABLE IF NOT EXISTS terminal.stuffingterminals (
stuffingterminalid INT PRIMARY KEY AUTO_INCREMENT,
stuffingterminalname VARCHAR (100) NOT NULL);

-- Exporters table --
CREATE TABLE IF NOT EXISTS terminal.exporters (
exporterid INT PRIMARY KEY AUTO_INCREMENT,
exportername VARCHAR (100) NOT NULL);

-- Fumigators table --
CREATE TABLE IF NOT EXISTS terminal.fumigators (
fumigatorid INT PRIMARY KEY AUTO_INCREMENT,
fumigatorname VARCHAR (100) NOT NULL);

-- Supervisors table --
CREATE TABLE IF NOT EXISTS terminal.supervisors (
supervisorid INT PRIMARY KEY AUTO_INCREMENT,
supervisorname VARCHAR (100) NOT NULL);

-- Custom Brokers table --
CREATE TABLE IF NOT EXISTS terminal.custombrokers (
custombrokerid INT PRIMARY KEY AUTO_INCREMENT,
custombrokername VARCHAR (100) NOT NULL);

-- Products table --
CREATE TABLE IF NOT EXISTS terminal.products (
productid INT PRIMARY KEY AUTO_INCREMENT,
producttype VARCHAR (50) NOT NULL);

-- Packlists table -- 
CREATE TABLE IF NOT EXISTS terminal.packlists (
packistid INT PRIMARY KEY AUTO_INCREMENT,
productid INT (10) NOT NULL,
packlistpurchase VARCHAR (20),
packlistsupplier VARCHAR (100),
packlistlot INT (10),
packlistbales INT (10),
packlistbaleshc INT (10),
packlistgrade INT (10),
packlistsittingnf VARCHAR (50),
packlistnetweight VARCHAR (20));

-- Terminal Steps table --
CREATE TABLE IF NOT EXISTS terminal.terminalsteps(
terminalstepsid INT PRIMARY KEY AUTO_INCREMENT,
processid INT NOT NULL,
workflowstepId INT NOT NULL,
createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
approved TINYINT DEFAULT 0 NOT NULL,
approvedat DATETIME DEFAULT CURRENT_TIMESTAMP,
reason VARCHAR(50));


-- Terminal User Steps table --
CREATE TABLE IF NOT EXISTS terminal.terminaluserssteps(
terminalusersstepsid INT PRIMARY KEY AUTO_INCREMENT,
processid INT NOT NULL,
terminalstepsid INT NOT NULL,
userid INT,
groupid INT,
stepuserroleId INT NOT NULL,
createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL);

-- --
-- Foreign Keys -- 
-- --

-- Processes -> Stuffing Terminals FK -- 
ALTER TABLE terminal.processes ADD CONSTRAINT processes_stuffing_terminal_FK FOREIGN KEY (stuffingterminalid) REFERENCES terminal.stuffingterminals(stuffingterminalid) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Processes -> Exporters FK --
ALTER TABLE terminal.processes ADD CONSTRAINT processes_exporters_FK FOREIGN KEY (exporterid) REFERENCES terminal.exporters(exporterid) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Processes -> Fumigators FK --
ALTER TABLE terminal.processes ADD CONSTRAINT processes_fumigators_FK FOREIGN KEY (fumigatorid) REFERENCES terminal.fumigators(fumigatorid) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Processes -> Supervisors FK -- 
ALTER TABLE terminal.processes ADD CONSTRAINT processes_supervisors_FK FOREIGN KEY (supervisorid) REFERENCES terminal.supervisors(supervisorid) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Processes -> Custom Brokers FK -- 
ALTER TABLE terminal.processes ADD CONSTRAINT processes_custombrokers_FK FOREIGN KEY (custombrokerid) REFERENCES terminal.custombrokers(custombrokerid) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Processes -> Products FK -- 
ALTER TABLE terminal.processes ADD CONSTRAINT processes_products_FK FOREIGN KEY (productid) REFERENCES terminal.products(productid) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Processes -> Workflows FK --
ALTER TABLE terminal.processes ADD CONSTRAINT processes_workflows_FK FOREIGN KEY (workflowid) REFERENCES workflows.workflows(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Processes -> Voyages FK --
ALTER TABLE terminal.processes ADD CONSTRAINT processes_voyages_FK FOREIGN KEY (voyagesid) REFERENCES shipments.voyages(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Processes -> Companies FK --
ALTER TABLE terminal.processes ADD CONSTRAINT processes_companies_FK FOREIGN KEY (companyid) REFERENCES users.companies(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Processes -> Company Vessels FK (ShipOwner) --
ALTER TABLE terminal.processes ADD CONSTRAINT processes_shipowners_FK FOREIGN KEY (shipownerid) REFERENCES shipments.vesselcompanies(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Processes -> Container Types FK --
ALTER TABLE terminal.processes ADD CONSTRAINT processes_containertypes_FK FOREIGN KEY (containertypeid) REFERENCES shipments.containertypes(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Terminal Steps -> Processes FK --
ALTER TABLE terminal.terminalsteps ADD CONSTRAINT terminalsteps_processes_FK FOREIGN KEY (processid) REFERENCES terminal.processes(processid) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Terminal Steps -> Workflow Steps FK --
ALTER TABLE terminal.terminalsteps ADD CONSTRAINT terminalsteps_workflowsteps_FK FOREIGN KEY (workflowstepId) REFERENCES workflows.workflowsteps(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Terminal Users Steps -> Processes FK --
ALTER TABLE terminal.terminaluserssteps ADD CONSTRAINT terminaluserssteps_processesFK FOREIGN KEY (processid) REFERENCES terminal.processes(processid) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Terminal Users Steps -> Groups FK --
ALTER TABLE terminal.terminaluserssteps ADD CONSTRAINT terminaluserssteps_groups_FK FOREIGN KEY (groupid) REFERENCES users.groups(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Terminal Users Steps -> Users FK --
ALTER TABLE terminal.terminaluserssteps ADD CONSTRAINT terminaluserssteps_users_FK FOREIGN KEY (userid) REFERENCES users.users(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Terminal Users Steps -> Step User Roles FK --
ALTER TABLE terminal.terminaluserssteps ADD CONSTRAINT terminaluserssteps_workflowstepuserroles_FK FOREIGN KEY (stepuserroleId) REFERENCES workflows.stepuserroles(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Terminal Users Steps -> Workflow Steps FK -- 
ALTER TABLE terminal.terminaluserssteps ADD CONSTRAINT terminaluserssteps_workflowsteps_FK FOREIGN KEY (terminalstepsid) REFERENCES workflows.workflowsteps(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- --
-- Base tables data --
-- --

-- Base Stuffing Terminals -- 
INSERT INTO terminal.stuffingterminals
(stuffingterminalId, stuffingterminalname)
VALUES(1, 'Stuffing Terminal 01');
INSERT INTO terminal.stuffingterminals
(stuffingterminalId, stuffingterminalname)
VALUES(2, 'Stuffing Terminal 02');
INSERT INTO terminal.stuffingterminals
(stuffingterminalId, stuffingterminalname)
VALUES(3, 'Stuffing Terminal 03');
INSERT INTO terminal.stuffingterminals
(stuffingterminalId, stuffingterminalname)
VALUES(4, 'Stuffing Terminal 04');
INSERT INTO terminal.stuffingterminals
(stuffingterminalId, stuffingterminalname)
VALUES(5, 'Stuffing Terminal 05');

-- Base Exporters --
INSERT INTO terminal.exporters
(exporterid, exportername)
VALUES(1, 'Terminal Exporter 01');
INSERT INTO terminal.exporters
(exporterid, exportername)
VALUES(2, 'Terminal Exporter 02');
INSERT INTO terminal.exporters
(exporterid, exportername)
VALUES(3, 'Terminal Exporter 03');
INSERT INTO terminal.exporters
(exporterid, exportername)
VALUES(4, 'Terminal Exporter 04');
INSERT INTO terminal.exporters
(exporterid, exportername)
VALUES(5, 'Terminal Exporter 05');

-- Base Fumigators --
INSERT INTO terminal.fumigators
(fumigatorid, fumigatorname)
VALUES(1, 'Terminal Fumigator 01');
INSERT INTO terminal.fumigators
(fumigatorid, fumigatorname)
VALUES(2, 'Terminal Fumigator 02');
INSERT INTO terminal.fumigators
(fumigatorid, fumigatorname)
VALUES(3, 'Terminal Fumigator 03');
INSERT INTO terminal.fumigators
(fumigatorid, fumigatorname)
VALUES(4, 'Terminal Fumigator 04');
INSERT INTO terminal.fumigators
(fumigatorid, fumigatorname)
VALUES(5, 'Terminal Fumigator 05');

-- Base Supervisors --
INSERT INTO terminal.supervisors
(supervisorid, supervisorname)
VALUES(1, 'Supervisor 01');
INSERT INTO terminal.supervisors
(supervisorid, supervisorname)
VALUES(2, 'Supervisor 02');
INSERT INTO terminal.supervisors
(supervisorid, supervisorname)
VALUES(3, 'Supervisor 03');
INSERT INTO terminal.supervisors
(supervisorid, supervisorname)
VALUES(4, 'Supervisor 04');
INSERT INTO terminal.supervisors
(supervisorid, supervisorname)
VALUES(5, 'Supervisor 05');

-- Base Custom Brokers --
INSERT INTO terminal.custombrokers
(custombrokerid, custombrokername)
VALUES(1, 'Custom Broker 01');
INSERT INTO terminal.custombrokers
(custombrokerid, custombrokername)
VALUES(2, 'Custom Broker 02');
INSERT INTO terminal.custombrokers
(custombrokerid, custombrokername)
VALUES(3, 'Custom Broker 03');
INSERT INTO terminal.custombrokers
(custombrokerid, custombrokername)
VALUES(4, 'Custom Broker 04');
INSERT INTO terminal.custombrokers
(custombrokerid, custombrokername)
VALUES(5, 'Custom Broker 05');


-- Base Products --
INSERT INTO terminal.products
(productid, producttype)
VALUES(1, 'cotton');
INSERT INTO terminal.products
(productid, producttype)
VALUES(2, 'sugar');
INSERT INTO terminal.products
(productid, producttype)
VALUES(3, 'sugar cane');
INSERT INTO terminal.products
(productid, producttype)
VALUES(4, 'beans');
INSERT INTO terminal.products
(productid, producttype)
VALUES(5, 'rice');

-- -- Base Processes --
-- INSERT INTO terminal.processes
-- (processid, companyid, exporterid, workflowid, contractnumber, stuffingterminalid, custombrokerid, supervisorid, fumigatorid, shipownerid, containertypeid, containerquantity, detentiondays, depotterminal, voyagesid, productid, productquantity, productweight)
-- VALUES(1, 1, 1, 1, 23, 1, 1, 1, 1, 1, 1, 2, 3, 'Depot 1', 1, 1, 10, 1);
-- INSERT INTO terminal.processes
-- (processid, companyid, exporterid, workflowid, contractnumber, stuffingterminalid, custombrokerid, supervisorid, fumigatorid, shipownerid, containertypeid, containerquantity, detentiondays, depotterminal, voyagesid, productid, productquantity, productweight)
-- VALUES(2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 'Depot 2', 2, 2, 20, 2);
-- INSERT INTO terminal.processes
-- (processid, companyid, exporterid, workflowid, contractnumber, stuffingterminalid, custombrokerid, supervisorid, fumigatorid, shipownerid, containertypeid, containerquantity, detentiondays, depotterminal, voyagesid, productid, productquantity, productweight)
-- VALUES(3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 3, 'Depot 3',  3, 3, 30, 3);
-- INSERT INTO terminal.processes
-- (processid, companyid, exporterid, workflowid, contractnumber, stuffingterminalid, custombrokerid, supervisorid, fumigatorid, shipownerid, containertypeid, containerquantity, detentiondays, depotterminal, voyagesid, productid, productquantity, productweight)
-- VALUES(4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 3, 'Depot 4', 4, 4, 40, 4);
-- INSERT INTO terminal.processes
-- (processid, companyid, exporterid, workflowid, contractnumber, stuffingterminalid, custombrokerid, supervisorid, fumigatorid, shipownerid, containertypeid, containerquantity, detentiondays, depotterminal, voyagesid, productid, productquantity, productweight)
-- VALUES(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 2, 3, 'Depot 5', 5, 5, 50, 5);

