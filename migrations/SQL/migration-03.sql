-- Migration que contempla as tabelas para abertura de um terminal --

Use terminal;

-- Tabela Terminals --
CREATE TABLE IF NOT EXISTS terminal.terminals (
terminalid INT PRIMARY KEY AUTO_INCREMENT,
terminalname VARCHAR(50) NOT NULL,
terminaladress VARCHAR(100) NOT NULL,
terminalcnpj VARCHAR(20) NOT NULL,
createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL);

-- Tabela Terminal Contacts --
CREATE TABLE IF NOT EXISTS terminal.terminalcontacts (
terminalcontactsid INT PRIMARY KEY AUTO_INCREMENT,
terminalid INT NOT NULL,
terminalcontactname VARCHAR(50) NOT NULL,
terminalcontactemail VARCHAR(100) NOT NULL,
createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL);

-- Tabela Terminal Periods --
CREATE TABLE IF NOT EXISTS terminal.terminalperiods (
terminalperiodsid INT PRIMARY KEY AUTO_INCREMENT,
terminalid INT NOT NULL,
terminalperiodname VARCHAR(50) NOT NULL,
terminalperiodstarttime TIME NOT NULL,
terminalperiodendtime TIME,
createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL);

-- Tabela Terminal Service Grids -- 
CREATE TABLE IF NOT EXISTS terminal.terminalservicegrids (
terminalservicegridid INT PRIMARY KEY AUTO_INCREMENT,
terminalperiodid INT NOT NULL,
terminalservicegridvacancies INT(10) NOT NULL,
servicegridexceptionid INT NOT NULL,
createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL);

-- Tabela Service Grid Exceptions --
CREATE TABLE IF NOT EXISTS terminal.servicegridexceptions (
servicegridexceptionid INT PRIMARY KEY AUTO_INCREMENT,
servicegridexceptiondescription VARCHAR(50) NOT NULL,
createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL);

-- Tabela Months --
CREATE TABLE IF NOT EXISTS terminal.months (
monthid INT PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(20) NOT NULL,
createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL);

-- Tabela Service Grid Months --
CREATE TABLE IF NOT EXISTS terminal.servicegridmonths(
servicegridmonthsid INT PRIMARY KEY AUTO_INCREMENT,
servicegridid INT NOT NULL,
monthid INT NOT NULL,
createdat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
updatedat DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL);


-- Foreign Keys --

-- Service Grid Months -> Service Grids Fk --
ALTER TABLE terminal.servicegridmonths ADD CONSTRAINT servicegridmonths_servicegrids_FK FOREIGN KEY (servicegridid) REFERENCES terminal.terminalservicegrids(terminalservicegridid) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Service Grid Months -> Months Fk --

ALTER TABLE terminal.servicegridmonths ADD CONSTRAINT servicegridmonths_months_FK FOREIGN KEY (monthid) REFERENCES terminal.months(monthid) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Service Grids -> Service Grid Exceptions Fk --

ALTER TABLE terminal.terminalservicegrids ADD CONSTRAINT terminalservicegrids_servicegridexceptions_FK FOREIGN KEY (servicegridexceptionid) REFERENCES terminal.servicegridexceptions(servicegridexceptionid) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Terminal Periods -> Terminal Fk --

ALTER TABLE terminal.terminalperiods ADD CONSTRAINT terminalperiods_terminals_FK FOREIGN KEY (terminalid) REFERENCES terminal.terminals(terminalid) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Service Contacts -> Terminal Periods Fk --

ALTER TABLE terminal.terminalcontacts ADD CONSTRAINT terminalcontacts_terminals_FK FOREIGN KEY (terminalid) REFERENCES terminal.terminals(terminalid) ON DELETE RESTRICT ON UPDATE RESTRICT;
