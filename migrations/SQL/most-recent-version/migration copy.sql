
-- --
-- Base tables data --
-- --

-- Base Stuffing Terminals -- 
INSERT INTO Terminal.stuffingterminals
(id, stuffingterminalname)
VALUES(1, 'Stuffing Terminal 01');
INSERT INTO Terminal.stuffingterminals
(id, stuffingterminalname)
VALUES(2, 'Stuffing Terminal 02');
INSERT INTO Terminal.stuffingterminals
(id, stuffingterminalname)
VALUES(3, 'Stuffing Terminal 03');
INSERT INTO Terminal.stuffingterminals
(id, stuffingterminalname)
VALUES(4, 'Stuffing Terminal 04');
INSERT INTO Terminal.stuffingterminals
(id, stuffingterminalname)
VALUES(5, 'Stuffing Terminal 05');

-- Base Exporters --
INSERT INTO Terminal.exporters
(id, exportername)
VALUES(1, 'Terminal Exporter 01');
INSERT INTO Terminal.exporters
(id, exportername)
VALUES(2, 'Terminal Exporter 02');
INSERT INTO Terminal.exporters
(id, exportername)
VALUES(3, 'Terminal Exporter 03');
INSERT INTO Terminal.exporters
(id, exportername)
VALUES(4, 'Terminal Exporter 04');
INSERT INTO Terminal.exporters
(id, exportername)
VALUES(5, 'Terminal Exporter 05');

-- Base Fumigators --
INSERT INTO Terminal.fumigators
(id, fumigatorname)
VALUES(1, 'Terminal Fumigator 01');
INSERT INTO Terminal.fumigators
(id, fumigatorname)
VALUES(2, 'Terminal Fumigator 02');
INSERT INTO Terminal.fumigators
(id, fumigatorname)
VALUES(3, 'Terminal Fumigator 03');
INSERT INTO Terminal.fumigators
(id, fumigatorname)
VALUES(4, 'Terminal Fumigator 04');
INSERT INTO Terminal.fumigators
(id, fumigatorname)
VALUES(5, 'Terminal Fumigator 05');

-- Base Supervisors --
INSERT INTO Terminal.supervisors
(id, supervisorname)
VALUES(1, 'Supervisor 01');
INSERT INTO Terminal.supervisors
(id, supervisorname)
VALUES(2, 'Supervisor 02');
INSERT INTO Terminal.supervisors
(id, supervisorname)
VALUES(3, 'Supervisor 03');
INSERT INTO Terminal.supervisors
(id, supervisorname)
VALUES(4, 'Supervisor 04');
INSERT INTO Terminal.supervisors
(id, supervisorname)
VALUES(5, 'Supervisor 05');

-- Base Custom Brokers --
INSERT INTO Terminal.custombrokers
(id, custombrokername)
VALUES(1, 'Custom Broker 01');
INSERT INTO Terminal.custombrokers
(id, custombrokername)
VALUES(2, 'Custom Broker 02');
INSERT INTO Terminal.custombrokers
(id, custombrokername)
VALUES(3, 'Custom Broker 03');
INSERT INTO Terminal.custombrokers
(id, custombrokername)
VALUES(4, 'Custom Broker 04');
INSERT INTO Terminal.custombrokers
(id, custombrokername)
VALUES(5, 'Custom Broker 05');


-- Base Products --
INSERT INTO Terminal.products
(id, producttype)
VALUES(1, 'cotton');
INSERT INTO Terminal.products
(id, producttype)
VALUES(2, 'sugar');
INSERT INTO Terminal.products
(id, producttype)
VALUES(3, 'sugar cane');
INSERT INTO Terminal.products
(id, producttype)
VALUES(4, 'beans');
INSERT INTO Terminal.products
(id, producttype)
VALUES(5, 'rice');

-- -- Base Processes --
 INSERT INTO Terminal.processes
 (id, companyid, exporterid, workflowid, contractnumber, stuffingterminalid, custombrokerid, supervisorid, fumigatorid, shipownerid, containertypeid, containerquantity, detentiondays, depotterminal, voyagesid, productid, productquantity, productweight)
 VALUES(1, 1, 1, 1, 23, 1, 1, 1, 1, 1, 1, 2, 3, 'Depot 1', 1, 1, 10, 1);
 INSERT INTO Terminal.processes
 (id, companyid, exporterid, workflowid, contractnumber, stuffingterminalid, custombrokerid, supervisorid, fumigatorid, shipownerid, containertypeid, containerquantity, detentiondays, depotterminal, voyagesid, productid, productquantity, productweight)
 VALUES(2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 'Depot 2', 2, 2, 20, 2);
 INSERT INTO Terminal.processes
 (id, companyid, exporterid, workflowid, contractnumber, stuffingterminalid, custombrokerid, supervisorid, fumigatorid, shipownerid, containertypeid, containerquantity, detentiondays, depotterminal, voyagesid, productid, productquantity, productweight)
 VALUES(3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 3, 'Depot 3',  3, 3, 30, 3);
 INSERT INTO Terminal.processes
 (id, companyid, exporterid, workflowid, contractnumber, stuffingterminalid, custombrokerid, supervisorid, fumigatorid, shipownerid, containertypeid, containerquantity, detentiondays, depotterminal, voyagesid, productid, productquantity, productweight)
 VALUES(4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 3, 'Depot 4', 4, 4, 40, 4);
 INSERT INTO Terminal.processes
 (id, companyid, exporterid, workflowid, contractnumber, stuffingterminalid, custombrokerid, supervisorid, fumigatorid, shipownerid, containertypeid, containerquantity, detentiondays, depotterminal, voyagesid, productid, productquantity, productweight)
 VALUES(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 2, 3, 'Depot 5', 5, 5, 50, 5);

